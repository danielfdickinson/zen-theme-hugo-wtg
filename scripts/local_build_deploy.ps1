# cspell:ignore curdir
Remove-Item -Path public -Recurse -Force

$Env:URL="https://www.zen-theme-hugo-wtg.wtg-demos.ca/"
$URL="https://www.zen-theme-hugo-wtg.wtg-demos.ca/"
$Env:HUGO_PARAMS_DEPLOYEDBASEURL="$URL"
$Env:BASEURL="$URL"
$BASEURL = "$URL"
$curdir = Get-Location
$Env:HUGO_RESOURCEDIR="$curdir\resources"
hugo.exe --gc --minify -b $BASEURL --source exampleSite --destination "$curdir\public" --config "$curdir\exampleSite\config.yaml"
rclone sync --checksum --progress .\public\ wtgdeml-dfdzen:./
